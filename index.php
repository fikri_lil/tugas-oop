<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $sheep = new Animal("shaun");
   
    echo "Name: " . $sheep->name. "<br>"; 
    echo "Legs: " . $sheep->legs . "<br>";
    echo "cold_blooded: " . $sheep->cold_blooded . "<br><br>"; 

    $kodok = new Frog("buduk");
    echo "Name: " . $kodok->name. "<br>"; 
    echo "Legs: " . $kodok->legs . "<br>";
    echo "cold_blooded: " . $kodok->cold_blooded . "<br>"; 
    echo $kodok->Jump() . "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Name: " . $kodok->name. "<br>"; 
    echo "Legs: " . $kodok->legs . "<br>";
    echo "cold_blooded: " . $kodok->cold_blooded . "<br>"; 
    echo $sungokong->Yell();

?>
